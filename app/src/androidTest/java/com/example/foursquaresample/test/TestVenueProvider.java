package com.example.foursquaresample.test;

import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.test.AndroidTestCase;

import com.example.foursquaresample.data.VenueContract.VenueEntry;

import java.util.Map;
import java.util.Set;

/**
 * Junit test for data.VenueProvider
 */
public class TestVenueProvider extends AndroidTestCase {
    public static final String LOG_TAG = TestVenueProvider.class.getSimpleName();

    private static final Object[][] FAKE_DATA = new Object[][]{
            {"123456", "First Cafe", "Fake st 1", "012345678", 40.7d, -74d},
            {"123457", "Second Cafe", "Fake ave 12", "123456789", 40.69998d, -74.00001d},
            {"123458", "Third Bar", "Fake st 199", null, 40.7101d, -74d},
            {"123459", "Fourth Cafe", "Far st 13", "987654321", 40.701d, -74.2d},
            {"123460", "Far Bar", "Far far away 997", null, 40.72d, -74.4d}
    };

    // brings our database to an empty state
    public void deleteAllRecords() {
        mContext.getContentResolver().delete(
                VenueEntry.CONTENT_URI,
                null,
                null
        );

        Cursor cursor = mContext.getContentResolver().query(
                VenueEntry.CONTENT_URI,
                null,
                null,
                null,
                null
        );
        assertEquals(0, cursor.getCount());
        cursor.close();
    }

    // Since we want each test to start with a clean slate, run deleteAllRecords
    // in setUp (called by the test runner before each test).
    public void setUp() {
        deleteAllRecords();
    }


    public void testInsertReadProvider() {
        ContentValues locationValues = getFakeVenueValues(0);

        Uri venueInsertUri = mContext.getContentResolver().insert(VenueEntry.CONTENT_URI, locationValues);
        assertTrue(venueInsertUri != null);
        long venueRowId = ContentUris.parseId(venueInsertUri);

        // Data should be inserted. Now fetch it and compare with what we sent.
        Cursor cursor = mContext.getContentResolver().query(
                VenueEntry.CONTENT_URI,
                null, // leaving "columns" null just returns all the columns.
                null, // cols for "where" clause
                null, // values for "where" clause
                null  // sort order
        );
        validateCursor(cursor, locationValues);

        // Now see if we can successfully query if we include the row id
        cursor = mContext.getContentResolver().query(
                VenueEntry.buildVenueUri(venueRowId),
                null, // leaving "columns" null just returns all the columns.
                null, // cols for "where" clause
                null, // values for "where" clause
                null  // sort order
        );
        validateCursor(cursor, locationValues);

        // Next we insert all our rest fake records and then will check for our query that orders by distance
        for (int i = 1; i < FAKE_DATA.length; i++) {
            locationValues = getFakeVenueValues(i);
            venueInsertUri = mContext.getContentResolver().insert(VenueEntry.CONTENT_URI, locationValues);
            assertTrue(venueInsertUri != null);
        }

        cursor = mContext.getContentResolver().query(
                VenueEntry.buildVenueWithLocation(40.7d, -74d),
                null, // leaving "columns" null just returns all the columns.
                null, // cols for "where" clause
                null, // values for "where" clause
                null  // sort order
        );

        // records should be ordered, so just compare distances
        assertTrue(cursor.moveToFirst());
        float prevDistance = 0;
        do {
            String foursquareId = cursor.getString(cursor.getColumnIndex(VenueEntry.COLUMN_FOURSQUARE_ID));
            String name = cursor.getString(cursor.getColumnIndex(VenueEntry.COLUMN_NAME));
            String address = cursor.getString(cursor.getColumnIndex(VenueEntry.COLUMN_ADDRESS));
            String phone = cursor.getString(cursor.getColumnIndex(VenueEntry.COLUMN_PHONE));
            double lat = cursor.getDouble(cursor.getColumnIndex(VenueEntry.COLUMN_LAT));
            double lon = cursor.getDouble(cursor.getColumnIndex(VenueEntry.COLUMN_LON));
            float[] distance = new float[1];
            Location.distanceBetween(40.7d, -74d, lat, lon, distance);
            assertTrue(prevDistance <= distance[0]);
        } while (cursor.moveToNext());
        cursor.close();
    }

    private ContentValues getFakeVenueValues(int index) {
        // Create a new map of values, where column names are the keys
        ContentValues testValues = new ContentValues();
        testValues.put(VenueEntry.COLUMN_FOURSQUARE_ID, (String) FAKE_DATA[index][0]);
        testValues.put(VenueEntry.COLUMN_NAME, (String) FAKE_DATA[index][1]);
        testValues.put(VenueEntry.COLUMN_ADDRESS, (String) FAKE_DATA[index][2]);
        testValues.put(VenueEntry.COLUMN_PHONE, (String) FAKE_DATA[index][3]);
        testValues.put(VenueEntry.COLUMN_LAT, (Double) FAKE_DATA[index][4]);
        testValues.put(VenueEntry.COLUMN_LON, (Double) FAKE_DATA[index][5]);
        return testValues;
    }

    private void validateCursor(Cursor valueCursor, ContentValues expectedValues) {
        assertTrue(valueCursor.moveToFirst());
        validateCurrentRow(valueCursor, expectedValues);
        valueCursor.close();
    }

    private void validateCurrentRow(Cursor valueCursor, ContentValues expectedValues) {
        Set<Map.Entry<String, Object>> valueSet = expectedValues.valueSet();
        for (Map.Entry<String, Object> entry : valueSet) {
            String columnName = entry.getKey();
            int idx = valueCursor.getColumnIndex(columnName);
            assertFalse(idx == -1);
            if (columnName.equals(VenueEntry.COLUMN_LAT) || columnName.equals(VenueEntry.COLUMN_LON)) {
                double expectedValue = (Double) entry.getValue();
                assertEquals(expectedValue, valueCursor.getDouble(idx));
            } else {
                String expectedValue = entry.getValue().toString();
                assertEquals(expectedValue, valueCursor.getString(idx));
            }
        }
    }
}
