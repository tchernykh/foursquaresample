package com.example.foursquaresample.foursquare;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.location.Location;
import android.net.Uri;
import android.util.Log;

import com.example.foursquaresample.Utils;
import com.example.foursquaresample.data.Venue;
import com.example.foursquaresample.data.VenueContract.VenueEntry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Tools class for loading and parsing data from foursquare service.
 */
public class FoursquareLoader {

    private static final String LOG_TAG = FoursquareLoader.class.getSimpleName();

    // these two keys should be encoded in real app
    private static final String CLIENT_ID = "ACAO2JPKM1MXHQJCK45IIFKRFR2ZVL0QASMCBCG5NPJQWF2G";
    private static final String CLIENT_SECRET = "YZCKUYJ1WHUV2QICBXUBEILZI1DMPUIDP5SHV043O04FKBHL";
    private static final String API_VERSION = "20140927";

    // url parameter names to construct query url
    private static final String FORESQUARE_BASE_URL = "https://api.foursquare.com/v2/venues/search";
    private static final String COORDINATES_PARAM = "ll";
    private static final String QUERY_PARAM = "query";
    private static final String LIMIT_PARAM = "limit";
    private static final String CLIENT_ID_PARAM = "client_id";
    private static final String CLIENT_SECRET_PARAM = "client_secret";
    private static final String VERSION_PARAM = "v";

    // values for search query
    private static final String SEARCH_QUERY = "coffee";
    private static final String SEARCH_LIMIT = "50";

    // json keys
    private static final String JSON_FOURSQUARE_ID_KEY = "id";
    private static final String JSON_NAME_KEY = "name";
    private static final String JSON_LOCATION_KEY = "location";
    private static final String JSON_FORMATTED_ADDRESS_KEY = "formattedAddress";
    private static final String JSON_DISTANCE_KEY = "distance";
    private static final String JSON_LAT_KEY = "lat";
    private static final String JSON_LNG_KEY = "lng";
    private static final String JSON_RESPONSE_KEY = "response";
    private static final String JSON_VENUES_KEY = "venues";
    private static final String JSON_CONTACT_KEY = "contact";
    private static final String JSON_PHONE_KEY = "phone";

    /**
     * Load venues json from foursquare and parses it into list of venues
     * @param lat - lattitude
     * @param lon - longitude
     * @return list of parsed venues
     */
    public List<Venue> loadVenues(Context context, final double lat, final double lon) {
        if (!Utils.isNetworkAvailable(context)) return null;
        String venuesJsonStr = loadVenuesJsonFromFoursquare(lat, lon);
        if (venuesJsonStr == null) return null;

        List<Venue> venuesValues = null;
        try {
            venuesValues = getVenuesDataFromJson(venuesJsonStr);
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
        }

        storeVenuesData(context, venuesValues);
        return venuesValues;
    }

    private void storeVenuesData(Context context, List<Venue> venuesValues) {
        List<ContentValues> valuesToStore = new ArrayList<ContentValues>(venuesValues.size());
        for (Venue venue : venuesValues) {
            ContentValues cv = new ContentValues();
            cv.put(VenueEntry.COLUMN_FOURSQUARE_ID, venue.getFoursquareId());
            cv.put(VenueEntry.COLUMN_NAME, venue.getName());
            cv.put(VenueEntry.COLUMN_ADDRESS, venue.getAddress());
            cv.put(VenueEntry.COLUMN_PHONE, venue.getPhoneNumber());
            cv.put(VenueEntry.COLUMN_LON, venue.getLon());
            cv.put(VenueEntry.COLUMN_LAT, venue.getLat());
            valuesToStore.add(cv);
        }

        // store venues data, so all UI will be updated automatically
        if (valuesToStore.size() > 0) {
            final ContentResolver contentResolver = context.getContentResolver();
            contentResolver.bulkInsert(VenueEntry.CONTENT_URI,
                    valuesToStore.toArray(new ContentValues[valuesToStore.size()]));
            // TODO probably we could delete some old data, for example those which were not updated
            // in last month - to do this we need to add new field with last data update time
        }

    }

    /**
     * Load json for venues data from foursquare service using clientId and clientSecret
     * @param lat - lattitude
     * @param lon - longitude
     * @return json with venues data
     */
    public String loadVenuesJsonFromFoursquare(double lat, double lon) {
        String coordinates = lat + "," + lon;
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        // Will contain the raw JSON response as a string.
        String venuesJsonStr = null;
        try {
            // Construct the URL for the Foursquare search for venues query
            // Userless query: https://developer.foursquare.com/overview/auth#userless
            // Possible parameters are available at https://developer.foursquare.com/docs/venues/search
            Uri builtUri = Uri.parse(FORESQUARE_BASE_URL).buildUpon()
                    .appendQueryParameter(COORDINATES_PARAM, coordinates)
                    .appendQueryParameter(QUERY_PARAM, SEARCH_QUERY)
                    .appendQueryParameter(LIMIT_PARAM, SEARCH_LIMIT)
                    .appendQueryParameter(CLIENT_ID_PARAM, CLIENT_ID)
                    .appendQueryParameter(CLIENT_SECRET_PARAM, CLIENT_SECRET)
                    .appendQueryParameter(VERSION_PARAM, API_VERSION)
                    .build();

            URL url = new URL(builtUri.toString());
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuilder buffer = new StringBuilder();
            if (inputStream == null) {
                // Nothing to do.
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }

            if (buffer.length() == 0) {
                return null;
            }
            venuesJsonStr = buffer.toString();
        } catch (Exception e) {
            Log.e(LOG_TAG, "Unable to load venues", e);
        } finally{
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e(LOG_TAG, "Error closing stream", e);
                }
            }
        }
        return venuesJsonStr;
    }

    /**
     * Parses json and creates list of venues with data that we need.
     * @param venuesJsonStr - json to parse
     * @return list of parsed venues
     * @throws JSONException - wrong json format
     */
    public List<Venue> getVenuesDataFromJson(String venuesJsonStr) throws JSONException {
        // venues are under 'response' > 'venues'
        JSONArray venuesArray = new JSONObject(venuesJsonStr).getJSONObject(JSON_RESPONSE_KEY).getJSONArray(JSON_VENUES_KEY);
        List<Venue> result = new ArrayList<Venue>(venuesArray.length());
        StringBuilder addressBuilder = new StringBuilder();
        for (int i = 0; i < venuesArray.length(); i++) {
            JSONObject venueItem = venuesArray.getJSONObject(i);
            Venue venue = new Venue();
            try {
                String name = venueItem.getString(JSON_NAME_KEY);
                String foursquareId = venueItem.getString(JSON_FOURSQUARE_ID_KEY);
                // Address is under 'location' > 'address' but not all venues have it, however it
                // seems all venues have 'location' > 'formattedAddress' array of strings
                JSONObject venueLocation = venueItem.getJSONObject(JSON_LOCATION_KEY);
                JSONArray addressArray = venueLocation.getJSONArray(JSON_FORMATTED_ADDRESS_KEY);
                for (int j = 0; j < addressArray.length(); j++) {
                    addressBuilder.append(addressArray.getString(j));
                    if (j != (addressArray.length() - 1)) addressBuilder.append(",");
                }
                int distance = venueLocation.getInt(JSON_DISTANCE_KEY);
                double lat = venueLocation.getDouble(JSON_LAT_KEY);
                double lon = venueLocation.getDouble(JSON_LNG_KEY);
                if (venueItem.has(JSON_CONTACT_KEY)) {
                    JSONObject venueContacts = venueItem.getJSONObject(JSON_CONTACT_KEY);
                    if (venueContacts.has(JSON_PHONE_KEY)) {
                        String phoneNumber = venueContacts.getString(JSON_PHONE_KEY);
                        venue.setPhoneNumber(phoneNumber);
                    }
                }

                venue.setFoursquareId(foursquareId);
                venue.setName(name);
                venue.setAddress(addressBuilder.toString());
                // reuse object instead of creating one for better performance
                addressBuilder.setLength(0);
                venue.setDistance(distance);
                venue.setLat(lat);
                venue.setLon(lon);
            } catch (Exception ex) {
                Log.e(LOG_TAG, "Json parse error", ex);
            }
            result.add(venue);
        }
        return result;
    }

}
