package com.example.foursquaresample;

import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.foursquaresample.data.VenueContract;

/**
 * Fragment to show details about selected venue.
 */
public class VenueDetailsFragment extends Fragment implements View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    private static final int DETAIL_LOADER = 0;

    // details need more columns, so list them all here
    private static final String[] VENUE_COLUMNS = {
            VenueContract.VenueEntry._ID,
            VenueContract.VenueEntry.COLUMN_NAME,
            VenueContract.VenueEntry.COLUMN_ADDRESS,
            VenueContract.VenueEntry.COLUMN_PHONE,
            VenueContract.VenueEntry.COLUMN_LON,
            VenueContract.VenueEntry.COLUMN_LAT
    };

    // These indices are tied to VENUE_COLUMNS.  If VENUE_COLUMNS changes, these must change.
    public static final int COL_ID = 0;
    public static final int COL_NAME = 1;
    public static final int COL_ADDRESS = 2;
    public static final int COL_PHONE = 3;
    public static final int COL_LON = 4;
    public static final int COL_LAT = 5;

    private TextView mNameView;
    private TextView mDistanceView;
    private TextView mAddressView;
    private Button mCallButton;
    private Button mShowOnMapButton;

    private String mVenuePhone;
    private String mVenueName;
    private double mVenueLatitude;
    private double mVenueLongitude;

    public VenueDetailsFragment() {
    }

    public long getVenueId() {
        if (getArguments() == null) return -1;
        return getArguments().getLong(VenueDetailsActivity.VENUE_ID_KEY);
    }

    public double getLatitude() {
        if (getArguments() == null) return 0.0;
        return getArguments().getDouble(VenueDetailsActivity.LATITUDE_KEY);
    }

    public double getLongitudeId() {
        if (getArguments() == null) return 0.0;
        return getArguments().getDouble(VenueDetailsActivity.LONGITUDE_KEY);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getVenueId() >= 0) {
            // load our venue
            getLoaderManager().initLoader(DETAIL_LOADER, null, this);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_venue_details, container, false);

        mNameView = (TextView) rootView.findViewById(R.id.venue_name);
        mDistanceView = (TextView) rootView.findViewById(R.id.venue_distance);
        mAddressView = (TextView) rootView.findViewById(R.id.venue_address);
        mCallButton = (Button) rootView.findViewById(R.id.call);
        mShowOnMapButton = (Button) rootView.findViewById(R.id.show_on_map);

        mCallButton.setOnClickListener(this);
        mShowOnMapButton.setOnClickListener(this);
        return rootView;
    }

    public static VenueDetailsFragment newInstance(long venueId, double latitude, double longitude) {
        VenueDetailsFragment venueDetailsFragment = new VenueDetailsFragment();

        Bundle bundle = new Bundle();
        bundle.putLong(VenueDetailsActivity.VENUE_ID_KEY, venueId);
        bundle.putDouble(VenueDetailsActivity.LATITUDE_KEY, latitude);
        bundle.putDouble(VenueDetailsActivity.LONGITUDE_KEY, longitude);
        venueDetailsFragment.setArguments(bundle);

        return venueDetailsFragment;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.call: {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + mVenuePhone));
                if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(intent);
                } else {
                    Toast.makeText(getActivity(), "No app to call to phone number installed.", Toast.LENGTH_LONG).show();
                }
                break;
            }
            case R.id.show_on_map: {
                Uri location = Uri.parse("geo:" + mVenueLatitude + "," + mVenueLongitude + "(" + mVenueName + ")");
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(location);
                if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(intent);
                } else {
                    Toast.makeText(getActivity(), "No app to view location installed.", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(
                getActivity(),
                VenueContract.VenueEntry.buildVenueUri(getVenueId()),
                VENUE_COLUMNS, null, null, null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        if (cursor.moveToFirst()) {
            // set venue data to fields
            mVenueName = cursor.getString(COL_NAME);
            String address = cursor.getString(COL_ADDRESS);
            mVenuePhone = cursor.getString(COL_PHONE);
            mVenueLatitude = cursor.getDouble(COL_LAT);
            mVenueLongitude = cursor.getDouble(COL_LON);
            float[] distance = new float[1];
            Location.distanceBetween(getLatitude(), getLongitudeId(), mVenueLatitude, mVenueLongitude, distance);

            mNameView.setText(mVenueName);
            mDistanceView.setText(Utils.formatDistance(getActivity(), distance[0], false));
            mAddressView.setText(address);

            // show call button only when we have phone number
            mCallButton.setVisibility(mVenuePhone == null?View.GONE:View.VISIBLE);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }
}
