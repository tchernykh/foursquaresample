package com.example.foursquaresample;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.foursquaresample.data.Venue;
import com.example.foursquaresample.data.VenueContract.VenueEntry;
import com.example.foursquaresample.foursquare.FoursquareLoader;

import java.util.List;

/**
 * Fragment to show list of venues
 * TODO: if there are no records show message 'loading', if there is no internet connection as well
 * show 'connection unavailable' message
 */
public class VenuesListFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final String LOG_TAG = VenuesListFragment.class.getSimpleName();

    // we reload data only if user's location changed more than MIN_DIFF
    private static final double MIN_DIFF = 0.00001;

    private static final int VENUE_LOADER = 0;

    // fetch only those columns that we show in this list
    private static final String[] VENUE_COLUMNS = {
            VenueEntry._ID,
            VenueEntry.COLUMN_NAME,
            VenueEntry.COLUMN_ADDRESS,
            VenueEntry.COLUMN_LON,
            VenueEntry.COLUMN_LAT
    };

    // These indices are tied to VENUE_COLUMNS.  If VENUE_COLUMNS changes, these must change.
    public static final int COL_ID = 0;
    public static final int COL_NAME = 1;
    public static final int COL_ADDRESS = 2;
    public static final int COL_LON = 3;
    public static final int COL_LAT = 4;
    public static final String LATITUDE_KEY = "latitude";
    public static final String LONGITUDE_KEY = "longitude";

    private ListView mVenuesList;
    private VenuesAdapter mVenuesAdapter;

    // coordinates for last update
    private double mLatitude;
    private double mLongitude;

    public VenuesListFragment() {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(VENUE_LOADER, null, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_venues_list, container, false);
        mVenuesList = (ListView) rootView.findViewById(R.id.venues_list);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mLatitude = prefs.getFloat(LATITUDE_KEY, 0);
        mLongitude = prefs.getFloat(LONGITUDE_KEY, 0);
        mVenuesAdapter = new VenuesAdapter(getActivity(), null, 0, mLatitude, mLongitude);
        mVenuesList.setAdapter(mVenuesAdapter);
        mVenuesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CursorAdapter adapter = (CursorAdapter) parent.getAdapter();
                Cursor cursor = adapter.getCursor();
                if (cursor != null && cursor.moveToPosition(position)) {
                    long venueId = cursor.getLong(COL_ID);
                    final Intent intent = new Intent(view.getContext(), VenueDetailsActivity.class);
                    intent.putExtra(VenueDetailsActivity.VENUE_ID_KEY, venueId);
                    intent.putExtra(VenueDetailsActivity.LATITUDE_KEY, mLatitude);
                    intent.putExtra(VenueDetailsActivity.LONGITUDE_KEY, mLongitude);
                    view.getContext().startActivity(intent);
                }

            }
        });
        return rootView;
    }

    public void onLocationChanged(Location location) {
        // update only if user's location changed
        if (Math.abs(location.getLatitude() - mLatitude) > MIN_DIFF ||
                Math.abs(location.getLongitude() - mLongitude) > MIN_DIFF) {
            mLatitude = location.getLatitude();
            mLongitude = location.getLongitude();
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
            SharedPreferences.Editor editor = prefs.edit();
            editor.putFloat(LATITUDE_KEY, (float) mLatitude);
            editor.putFloat(LONGITUDE_KEY, (float) mLongitude);
            editor.commit();

            mVenuesAdapter.setLocation(mLatitude, mLongitude);
            // Update data from foursquare with new location
            new FetchVenuesTask().execute();
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
        // This is called when a new Loader needs to be created.  This
        // fragment only uses one loader, so we don't care about checking the id.
        Uri venueUri = VenueEntry.buildVenueWithLocation(mLatitude, mLongitude);

        // Now create and return a CursorLoader that will take care of
        // creating a Cursor for the data being displayed.
        return new CursorLoader(getActivity(), venueUri, VENUE_COLUMNS, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        mVenuesAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        mVenuesAdapter.swapCursor(null);
    }

    /**
     * Task to fetch venues from foursquare and set them to our adapter.
     */
    private class FetchVenuesTask extends AsyncTask<Void, Void, List<Venue>> {
        @Override
        protected List<Venue> doInBackground(Void... params) {
            return new FoursquareLoader().loadVenues(getActivity(), mLatitude, mLongitude);
        }

        @Override
        protected void onPostExecute(List<Venue> venues) {
            getLoaderManager().restartLoader(VENUE_LOADER, null, VenuesListFragment.this);
        }
    }
}