package com.example.foursquaresample;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

/**
 * An activity to show details of the venue with some actions like call or see on the map.
 */
public class VenueDetailsActivity extends ActionBarActivity {

    public static final String VENUE_ID_KEY = "venueId";

    public static final String LATITUDE_KEY = "latitude";

    public static final String LONGITUDE_KEY = "longitude";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venue_details);
        if (savedInstanceState == null) {
            Intent intent = getIntent();
            if (intent != null && intent.hasExtra(VENUE_ID_KEY)) {
                long venueId = intent.getLongExtra(VENUE_ID_KEY, -1);
                double latitude = intent.getDoubleExtra(LATITUDE_KEY, 0);
                double longitude = intent.getDoubleExtra(LONGITUDE_KEY, 0);
                // initialise fragment with data it uses
                VenueDetailsFragment df = VenueDetailsFragment.newInstance(venueId, latitude, longitude);
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.container, df)
                        .commit();
            }
        }
    }

}
