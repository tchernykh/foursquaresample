package com.example.foursquaresample.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Manages a local database for venues data
 */
public class VenueDbHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "venue.db";

    public VenueDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // Create a table to hold venues.
        final String SQL_CREATE_VENUE_TABLE = "CREATE TABLE " + VenueContract.VenueEntry.TABLE_NAME + " (" +
                VenueContract.VenueEntry._ID + " INTEGER PRIMARY KEY," +
                VenueContract.VenueEntry.COLUMN_FOURSQUARE_ID + " TEXT UNIQUE NOT NULL, " +
                VenueContract.VenueEntry.COLUMN_NAME + " TEXT NOT NULL, " +
                VenueContract.VenueEntry.COLUMN_ADDRESS + " TEXT NOT NULL, " +
                VenueContract.VenueEntry.COLUMN_PHONE + " TEXT, " +
                VenueContract.VenueEntry.COLUMN_LAT + " REAL NOT NULL, " +
                VenueContract.VenueEntry.COLUMN_LON + " REAL NOT NULL, " +
                " UNIQUE (" + VenueContract.VenueEntry.COLUMN_FOURSQUARE_ID + ") ON CONFLICT IGNORE);";
        sqLiteDatabase.execSQL(SQL_CREATE_VENUE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // not a big problem if we lose database data during upgrade - they will be reloaded
        // if network available
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + VenueContract.VenueEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
