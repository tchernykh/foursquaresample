package com.example.foursquaresample.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;
import com.example.foursquaresample.data.VenueContract.VenueEntry;

/**
 * Content provider for venues
 */
public class VenueProvider extends ContentProvider {

    private final String LOG_TAG = VenueProvider.class.getSimpleName();

    private static final int VENUE = 100;
    private static final int VENUE_ID = 101;
    private static final int VENUE_WITH_LOCATION = 102;

    private VenueDbHelper mOpenHelper;

    private static UriMatcher sUriMatcher = buildUriMatcher();

    // venues should be ordered by distance, so query should looks like this:
    // 'order by ((<lat> - LAT_COLUMN) * (<lat> - LAT_COLUMN) + (<lng> - LNG_COLUMN) * (<lng> - LNG_COLUMN) * <fudge>)'
    // where <fudge> = Math.pow(Math.cos(Math.toRadians(<lat>)),2);
    private static final String sVenueWithLocationOrder =
            "((? - " + VenueEntry.COLUMN_LAT + ") * (? - " + VenueEntry.COLUMN_LAT + ") +" +
                    " (? - " + VenueEntry.COLUMN_LON + ") * (? - " + VenueEntry.COLUMN_LON + ") * ?) ASC";

    private Cursor getVenuesOrderedByDistance(Uri uri, String[] projection, String selection) {
        String latitude = VenueEntry.getLatitudeFromUri(uri);
        String longitude = VenueEntry.getLongitudeFromUri(uri);
        double fudge = Math.pow(Math.cos(Math.toRadians(Double.parseDouble(latitude))),2);

        return mOpenHelper.getReadableDatabase().query(
                VenueEntry.TABLE_NAME,
                projection,
                selection,
                new String[] { latitude, latitude, longitude, longitude, String.valueOf(fudge) },
                null,
                null,
                sVenueWithLocationOrder
        );
    }

    @Override
    public boolean onCreate() {
        mOpenHelper = new VenueDbHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        // Here's the switch statement that, given a URI, will determine what kind of request it is,
        // and query the database accordingly.
        Cursor retCursor;
        switch (sUriMatcher.match(uri)) {
            // "venue/*/*"
            case VENUE_WITH_LOCATION:
            {
                retCursor = getVenuesOrderedByDistance(uri, projection, selection);
                break;
            }
            // "venue/*"
            case VENUE_ID: {
                retCursor = mOpenHelper.getReadableDatabase().query(
                        VenueEntry.TABLE_NAME,
                        projection,
                        VenueEntry._ID + " = ?",
                        new String[] {String.valueOf(ContentUris.parseId(uri)) },
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            // "venue"
            case VENUE: {
                retCursor = mOpenHelper.getReadableDatabase().query(
                        VenueEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;
    }

    @Override
    public String getType(Uri uri) {
        // Use the Uri Matcher to determine what kind of URI this is.
        final int match = sUriMatcher.match(uri);

        switch (match) {
            case VENUE:
                return VenueEntry.CONTENT_TYPE;
            case VENUE_ID:
                return VenueEntry.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri returnUri;

        switch (match) {
            case VENUE: {
                long _id = db.insert(VenueEntry.TABLE_NAME, null, contentValues);
                if ( _id > 0 )
                    returnUri = VenueEntry.buildVenueUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case VENUE:
                db.beginTransaction();
                int returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(VenueEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            default:
                return super.bulkInsert(uri, values);
        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);

        int affectedRows = 0;
        switch (match) {
            case VENUE:
                affectedRows = db.delete(VenueEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case VENUE_ID:
                affectedRows = db.delete(VenueEntry.TABLE_NAME,
                        VenueEntry._ID + " = " + ContentUris.parseId(uri), null);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        if (selection == null || affectedRows != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return affectedRows;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int affectedRows = 0;

        switch (match) {
            case VENUE: {
                affectedRows = db.update(VenueEntry.TABLE_NAME, contentValues, selection, selectionArgs);
                break;
            }
            case VENUE_ID:{
                affectedRows = db.update(VenueEntry.TABLE_NAME, contentValues,
                        VenueEntry._ID + " = " + ContentUris.parseId(uri), null);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (affectedRows != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return affectedRows;
    }

    /**
     * Builds uri matcher which is helpful to determine operation by uri
     * @return matcher
     */
    private static UriMatcher buildUriMatcher() {
        UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(VenueContract.CONTENT_AUTHORITY, VenueContract.PATH_VENUE, VENUE);
        matcher.addURI(VenueContract.CONTENT_AUTHORITY, VenueContract.PATH_VENUE + "/*", VENUE_ID);
        matcher.addURI(VenueContract.CONTENT_AUTHORITY, VenueContract.PATH_VENUE + "/*/*", VENUE_WITH_LOCATION);
        return matcher;
    }
}
