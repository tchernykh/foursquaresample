package com.example.foursquaresample.data;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Defines table and column names for the venues database.
 */
public class VenueContract {

    public static final String CONTENT_AUTHORITY = "com.example.foursquaresample";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_VENUE = "venue";

    public static final class VenueEntry implements BaseColumns {

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_VENUE;

        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_VENUE;

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_VENUE).build();

        public static final String TABLE_NAME = "venues";

        public static final String COLUMN_FOURSQUARE_ID = "foursquare_id";

        // Column for name
        public static final String COLUMN_NAME = "name";

        public static final String COLUMN_ADDRESS = "address";

        public static final String COLUMN_PHONE = "phone";

        public static final String COLUMN_LAT = "lat";

        public static final String COLUMN_LON = "lon";

        public static Uri buildVenueUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static String getLatitudeFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }

        public static String getLongitudeFromUri(Uri uri) {
            return uri.getPathSegments().get(2);
        }

        public static Uri buildVenueWithLocation(double latitude, double longitude) {
            return CONTENT_URI.buildUpon().appendPath(String.valueOf(latitude)).appendPath(String.valueOf(longitude)).build();
        }
    }
}
