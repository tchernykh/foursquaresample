package com.example.foursquaresample;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Utility methods
 */
public class Utils {

    public static String formatDistance(Context context, float distance, boolean shortFormat) {
        if (distance < 1000) {
            return context.getString(
                    shortFormat? R.string.format_distance_meters_short: R.string.format_distance_meters_long,
                    distance);
        } else {
            return context.getString(
                    shortFormat? R.string.format_distance_kilometers_short: R.string.format_distance_kilometers_long,
                    distance / 1000);
        }
    }

    /**
     * Check for network connection
     * @param context
     * @return is network connection available
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
