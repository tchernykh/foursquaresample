package com.example.foursquaresample;

import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
* Adapter to show venues in the list
*/
public class VenuesAdapter extends CursorAdapter {
    private static final String LOG_TAG = VenuesAdapter.class.getSimpleName();

    // coordinates for last update
    private double mLatitude;
    private double mLongitude;

    public void setLocation(double latitude, double longitude) {
        mLatitude = latitude;
        mLongitude = longitude;
    }

    public VenuesAdapter(Context context, Cursor c, int flags, double latitude, double longitude) {
        super(context, c, flags);
        setLocation(latitude, longitude);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        final View view = LayoutInflater.from(context).inflate(R.layout.list_item_venue, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        view.setTag(viewHolder);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder viewHolder = (ViewHolder) view.getTag();

        String name = cursor.getString(VenuesListFragment.COL_NAME);
        String address = cursor.getString(VenuesListFragment.COL_ADDRESS);
        double lat = cursor.getDouble(VenuesListFragment.COL_LAT);
        double lon = cursor.getDouble(VenuesListFragment.COL_LON);
        float[] distance = new float[1];
        Location.distanceBetween(mLatitude, mLongitude, lat, lon, distance);

        viewHolder.nameView.setText(name);
        viewHolder.distanceView.setText(Utils.formatDistance(context, distance[0], true));
        viewHolder.addressView.setText(address);
    }

    /**
     * Cache of the children views for a venue list item.
     */
    public static class ViewHolder {
        public final TextView nameView;
        public final TextView distanceView;
        public final TextView addressView;

        public ViewHolder(View view) {
            nameView = (TextView) view.findViewById(R.id.venue_name);
            distanceView = (TextView) view.findViewById(R.id.venue_distance);
            addressView = (TextView) view.findViewById(R.id.venue_address);
        }
    }
}
