FoursquareSample
============
This is a simple example android application that shows list of nearby venues where you could drink cofee.
It uses foursquare service as source of venues data.

It is an example of how to use following android api:

* location service from google play services which is optimal way currently (android 4.4)
* activity fragments
* content providers
* sqlite database
* junit tests

Environments
-------------
This application designed to work on android 2.3.3 and later.

System Dependencies & Configuration
-------------
 To develop this application you'll need:

 1. [Java 6.0 or newer](https://www.oracle.com/java/index.html)
 1. [Android Studio](https://developer.android.com/sdk/installing/studio.html) to develop project
 1. [Android SDK](http://developer.android.com/sdk/index.html) with following components installed using SDK Manager:
     - Android 4.4 platform
     - `Android Support Repository`
     - `Android Support Library`
     - `Google Play Services`
     - `Google Repository`
 1. [Gradle](http://www.gradle.org/downloads) v1.12+ to build project

Application Installation Instructions
-------------
* Instructions for Android Studio:
    1. Checkout project: `VCS` -> `Checkout from Version Control` -> `Git`
    1. Fill fields:
          - `Vcs Repository URL`: `https://bitbucket.org/tchernykh/foursquaresample.git`
          - `Parent Directory`: folder where project will be checkout
          - `Directory Name`: `foursquaresample`
    1. Next answer `yes` on question `...Would you like to open it?`
    1. Next select `Use default gradle wrapper` and click OK to import project from gradle
    1. Next open imported project and after automatic build will be finished you can just click run
    button to run the application or type Shift+F10.

* Instructions for Gradle console build
    1. Define the `ANDROID_HOME` environment variable which points to your Android SDK.
    1. Clone the project to your local computer by running command
    `git clone https://bitbucket.org/tchernykh/foursquaresample.git` in directory where you want to
    clone the project
    1. `cd foursquaresample` - go inside directory foursquaresample that were downloaded into the
    directory you specified
    1. run `./gradlew assembleDebug`
    1. At this point you should get BUILD SUCCESSFUL
    1. Now you can install the app with `adb install -r app/build/outputs/apk/apk-debug.apk`

- Note: Linux: Android SDK Tools may require installing `ia32-libs`
(See <http://developer.android.com/sdk/installing/index.html> -> Information for other platforms ->
Getting started on Linux -> Troubleshooting Ubuntu)
- Note2: Development with Eclipse is currently not possible because I am using the new
[project structure](http://developer.android.com/sdk/installing/studio-tips.html).

Usage Instructions
-------------
After you run the application on your android device you can:

 - view list of nearby coffee venues ordered by distance
 - click on a venue in the list to show more details and buttons:
   - call to this venue (if they have phone number)
   - view map where this venue located

Testing Instructions
-------------
 To test using Android Studio go to `com.example.foursquaresample.test.FullTestSuite` file and run it

What should be improved in future versions:
--------------
1. Implement tablet UI
1. Settings activity with setup for updates frequency and metric/imperial units
1. Show more data in list and details activity, especially images
1. Show on the map buttons should show path to the venue
1. Mark favourite places and share them to social networks with help of ShareActionProvider

History of development stages:
--------------
1. Generate app, add list of items, fake data - done
1. Add Foursquare api, fetch items, show real data, test fetching - done
1. Add VenueDetailsActivity which showed when users clicks on item with buttons to show on the map and call - done
1. Fetch data using user's location - done
1. Store fetched data to database, so the app could work offline - done
1. Create ContentProvider for venues data and us it instead of direct db calls - done
1. Save last location to settings and restore it when activity started - done
1. Improve UI - done
1. Write this readme - done